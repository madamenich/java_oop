package com.company;

public class Teacher extends Person{
    @Override
    void speak() {
        System.out.println("I am a teacher I can speak");
    }

    void teach(){
        System.out.println("I teach the student");
    }

    public Teacher(int age, String name, String gender) {

        super(age, name, gender);
        this.speak();
        this.teach();
        this.setAge(age);

    }

    @Override
    public void setAge(int age) {
        if ( age<25 ) System.out.println(" you're not allowed to teach");
        else System.out.println(" You can teach");
    }
}
