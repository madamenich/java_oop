package com.company;

public class Human {

    //data member
     int age;
     String name;
     String gender;

     //method
     void speak(){
         System.out.println("Human can speak");
     }

    void speak(String language){
        System.out.println("Human can speak "+language);
    }

     //constructor
    public Human() {
    }

    public Human(int age, String name, String gender) {
        this.age = age;
        this.name = name;
        this.gender = gender;
    }


    //getter setter
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Human{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
